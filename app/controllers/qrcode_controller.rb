class QrcodeController < ApplicationController
  def newqr
    @text = '{"presenceId": "'+qrcode_params[:presenceId]+'", "presenceNumber": "'+qrcode_params[:presenceNumber]+'"}'#qrcode_params.to_s
    @qr = RQRCode::QRCode.new(@text)
    png = @qr.as_png(
        resize_gte_to: false,
        resize_exactly_to: false,
        fill: 'white',
        color: 'black',
        size: 200,
        border_modules: 4,
        module_px_size: 6,
        file: nil
        #{}"./storage/"+qrcode_params[:presenceid].to_s+".png"
    )
    send_data png.to_s,type:'image/png', dispostion: 'inline'
  end


  def seeabsence
    response = HTTParty.get('https://fathomless-hollows-37188.herokuapp.com/check/presence/'+params[:class].to_s)
    @resp = response.body.as_json
    render json: @resp
  end


  private
  def qrcode_params
    params.require(:data).permit(:presenceId,:presenceNumber)
   end
end
