Rails.application.routes.draw do
  post 'qrcode/new' => 'qrcode#newqr'
  post 'presence/class' => 'qrcode#seeabsence'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
